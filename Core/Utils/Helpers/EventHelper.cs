﻿using System;
using System.Collections.Generic;
using caalhp.Core.Utils.Helpers.Serialization;
using caalhp.Core.Utils.Helpers.Serialization.JSON;

namespace caalhp.Core.Utils.Helpers
{
    public static class EventHelper
    {
        /*
        /// <summary>
        /// Creates a key string from a serializationtype and an event
        /// </summary>
        /// <typeparam name="T">type of the event</typeparam>
        /// <param name="serializationType">"json", "xml" or "string"</param>
        /// <param name="theEvent"></param>
        /// <returns></returns>
        /private static string CreateKeyFromEvent<T>(string serializationType, T theEvent)
        {
            return serializationType + ":" + theEvent.GetType();
        }*/

        /*public static string GetFullyQualifiedNameSpace(SerializationType stype, Type type)
        {
            var name = Enum.GetName(typeof(SerializationType), stype);
            if (name == null) return "";
            var part1 = name.ToLower() + ":";
            var part2 = type.FullName;
            return part1 + part2;
        }*/

        public static string GetFullyQualifiedNameSpace(SerializationType stype, Type type, string assemblyName = "CAALHP.Events")
        {
            var name = Enum.GetName(typeof(SerializationType), stype);
            if (name == null) return "";
            var serialization = name.ToLower();
            var classname = type.FullName;
            return String.Join(":", serialization, classname, assemblyName);
            //return serialization + classname + ":" + assemblyName;
        }

        public static KeyValuePair<string, string> CreateEvent<T>(SerializationType serializationType, T theEvent, string assemblyName = "CAALHP.Events")
        {
            var key = GetFullyQualifiedNameSpace(serializationType, theEvent.GetType(), assemblyName);
            
            
            string value;
            switch (serializationType)
            {
                case SerializationType.Json:
                    {
                        value = JsonSerializer.SerializeEvent(theEvent);
                    } break;
                default:
                    {
                        value = theEvent.ToString();
                    } break;
            }
            return new KeyValuePair<string, string>(key, value);
        }

        public static Type GetTypeFromFullyQualifiedNameSpace(string fullyQualifiedNameSpace)
        {
            var parts = fullyQualifiedNameSpace.Split(':');
            if (parts.Length == 3)
                if (!parts[2].Equals("CAALHP.Events"))
                {
                    return Type.GetType(parts[1] + ", " + parts[2]);
                }
                else
                {
                    return Type.GetType(parts[1]);
                }
            else return Type.GetType(parts[1] + ", CAALHP.Events");
        }


        public static SerializationType GetSerializationTypeFromFullyQualifiedNameSpace(string fullyQualifiedNameSpace)
        {
            var parts = fullyQualifiedNameSpace.Split(':');
            return Enum.Parse(typeof(SerializationType), parts[0]) is SerializationType ? (SerializationType)Enum.Parse(typeof(SerializationType), parts[0]) : SerializationType.String;
        }
    }
}
