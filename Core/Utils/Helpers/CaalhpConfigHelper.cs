﻿using System;
using System.IO;

namespace caalhp.Core.Utils.Helpers
{
    public static class CaalhpConfigHelper
    {
        public static Guid GetCaalhpId()
        {
            try
            {
                using (var reader = new StreamReader("config/caalhp.id"))
                {
                    var line = reader.ReadLine();
                    return line != null ? new Guid(line) : new Guid();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return new Guid();
        }

        public static string GetDefaultCulture()
        {
            try
            {
                using (var reader = new StreamReader("config/default.culture"))
                {
                    var line = reader.ReadLine();
                    return line;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return "";
        }
    }
}
