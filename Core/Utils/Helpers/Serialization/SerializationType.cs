﻿namespace caalhp.Core.Utils.Helpers.Serialization
{
    public enum SerializationType
    {
        Json, String, Xml
    }
}
