﻿namespace caalhp.Core.Contracts
{
    public interface IDeviceDriverHostCAALHPContract
    {
        IHostCAALHPContract Host { get; set; }
    }
}