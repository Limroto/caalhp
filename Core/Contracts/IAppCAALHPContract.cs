﻿namespace caalhp.Core.Contracts
{
    public interface IAppCAALHPContract : IBaseCAALHPContract
    {
        void Initialize(IAppHostCAALHPContract hostObj, int processId); 
        void Show();
    }
}
