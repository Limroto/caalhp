﻿namespace caalhp.Core.Contracts
{
    public interface IDeviceDriverCAALHPContract : IBaseCAALHPContract
    {
        void Initialize(IDeviceDriverHostCAALHPContract hostObj, int processId); 
        //double GetMeasurement();
    }
}
