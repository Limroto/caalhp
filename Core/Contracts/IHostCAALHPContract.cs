﻿using System.Collections.Generic;

namespace caalhp.Core.Contracts
{
    public interface IHostCAALHPContract
    {
        void ReportEvent(KeyValuePair<string, string> value);
        void SubscribeToEvents(string fullyQualifiedNameSpace, int processId);
        void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId);
    }
}
