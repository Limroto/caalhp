﻿namespace caalhp.Core.Contracts
{
    public interface IPluginInfo
    {
        string Name { get; set; }
        string LocationDir { get; set; }
    }
}