﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace caalhp.Core.Library.Config
{
    /// <summary>
    /// Used for keeping configuration information about a plugin.
    /// </summary>
    public class CareStorePluginConfig
    {
        public string PluginName { get; set; }
        [XmlArray]
        [XmlArrayItem(ElementName = "RunFile")]
        public List<string> RunFiles { get; set; }
        public string RunThisAfterInstall { get; set; }
        public bool RunAtStartup { get; set; }
        public bool RequiresLogin { get; set; }
        public bool RequiresFullTrust { get; set; }
        public bool AccessibleToCitizen { get; set; }
        public bool AccessibleToCareGiver { get; set; }
    }
}
