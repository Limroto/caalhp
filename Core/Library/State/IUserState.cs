﻿using System.Collections.Generic;
using caalhp.Core.Events.Types;
using caalhp.Core.Library.Config;

namespace caalhp.Core.Library.State
{
    public interface IUserState
    {
        void UserLoggedIn(User user);
        void UserLoggedOut();
        IList<PluginConfig> GetListOfInstalledApps();
    }
}