﻿using System;
using System.Collections.Generic;
using caalhp.Core.Events.Types;
using caalhp.Core.Library.Config;
using caalhp.Core.Library.Visitor;

namespace caalhp.Core.Library.State
{
    public class UserLoggedInState : IUserState
    {
        private readonly ICAALHP _caalhp;

        public UserLoggedInState(ICAALHP caalhp)
        {
            _caalhp = caalhp;
            Console.WriteLine("user logged in");
        }

        public void UserLoggedIn(User user)
        {

        }

        public void UserLoggedOut()
        {
            _caalhp.UserState = new NotLoggedInState(_caalhp);
        }

        public IList<PluginConfig> GetListOfInstalledApps()
        {
            var visitor = new GetListOfInstalledAppsUserLoggedInVisitor();
            _caalhp.Accept(visitor);
            return visitor.Result;
        }
    }
}