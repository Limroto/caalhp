﻿using System;
using caalhp.Core.Library.Observer;

namespace caalhp.Core.Library.Managers
{
    public class EventManager : IEventManager, IObservable<EventSubject>
    {
        private readonly EventSubject _eventSubject;

        public EventSubject EventSubject
        {
            get { return _eventSubject; }
        }

        public EventManager()
        {
            _eventSubject = new EventSubject();
        }

        public IDisposable Subscribe(IObserver<EventSubject> observer)
        {
            throw new NotImplementedException();
        }
    }
}
