﻿using caalhp.Core.Library.Observer;

namespace caalhp.Core.Library.Managers
{
    public interface IEventManager
    {
        EventSubject EventSubject { get; }
    }
}