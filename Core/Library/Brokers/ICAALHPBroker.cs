﻿using System.Collections.Generic;
using caalhp.Core.Library.Config;

namespace caalhp.Core.Library.Brokers
{
    public interface ICAALHPBroker
    {
        void ShowApp(string appName);
        void CloseApp(string appName);
        IList<PluginConfig> GetListOfInstalledApps();
        IList<PluginConfig> GetListOfInstalledDeviceDrivers();
        void ActivateDeviceDrivers();
        IList<PluginConfig> GetListOfRunningApps();
    }
}