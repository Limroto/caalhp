﻿using System.Collections.Generic;
using caalhp.Core.Library.Config;

namespace caalhp.Core.Library.Hosts
{
    public interface IDeviceDriverHost : IHost
    {
        void ActivateDrivers();
        //Dictionary<int, ProcessInfo> GetRunningDriversProcessInfo();
        List<string> GetListOfActiveDevices();
        void UpdateDriverList();
        IList<PluginConfig> GetLocalInstalledDeviceDrivers();
    }
}