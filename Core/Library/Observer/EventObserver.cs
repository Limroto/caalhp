﻿//using System.Collections.Generic;
//using CAALHP.Events;
//using CAALHP.Utils.Helpers;
//using CAALHP.Utils.Helpers.Serialization;

//namespace CAALHP.Library.Observer
//{
//    /// <summary>
//    /// The 'ConcreteObserver' class
//    /// </summary>
//    public class EventObserver : Observer
//    {
//        //private System.Collections.Generic.KeyValuePair<string, string> _observerState;
//        private EventSubject _subject;
//        private readonly IEventObserver _eventObserver;
//        //private readonly IHost _host;
//        private readonly string _fqns;

//        // Constructor
//        public EventObserver(EventSubject subject, IEventObserver eventObserver, string fqns, int id)
//            : base(id)
//        {
//            _subject = subject;
//            _eventObserver = eventObserver;
//            //_host = host;
//            _fqns = fqns;
//        }

//        public override void Update(KeyValuePair<string, string> value)
//        {

//            //_observerState = _subject.SubjectState;
//            _eventObserver.Update(value);
//            /*else
//            {
//                _subject.Notify(_fqns);
//            }*/
//            //_subject.Host.NotifyEventToPlugin(_observerState, Id);
//            //Console.WriteLine("Observer {0}'s new state is {1}", Id, _observerState);
//        }

//        /// <summary>
//        /// Special case update
//        /// </summary>
//        /// <param name="newFqns"></param>
//        public override void Update(string newFqns)
//        {
//            //_observerState = _subject.SubjectState;
//            var newEvent = new NewEventTypeAddedEvent
//            {
//                EventType = newFqns,
//                CallerName = GetType().FullName,
//                //CallerProcessId = Id
//            };
//            var value = EventHelper.CreateEvent(SerializationType.Json, newEvent);
//            _eventObserver.Update(value);
//            /*else
//            {
//                _subject.Notify(_fqns);
//            }*/
//        }

//    }

//    // Gets or sets subject
//    /*public EventSubject Subject
//    {
//        get { return _subject; }
//        set { _subject = value; }
//    }*/
//}
