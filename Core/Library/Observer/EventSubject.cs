﻿using System;
using caalhp.Core.Events;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization.JSON;

namespace caalhp.Core.Library.Observer
{
    public class EventSubject : Subject
    {
        public System.Collections.Generic.KeyValuePair<string, string> SubjectState { get; set; }
        
        public event UserLoggedInEventHandler UserLoggedIn;

        protected override void OnUserLoggedIn()
        {
            var handler = UserLoggedIn;
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(SubjectState.Key);
            var obj = JsonSerializer.DeserializeEvent(SubjectState.Value, type);
            if (obj.GetType() != typeof (UserLoggedInEvent)) return;
            if (handler != null) 
                handler(this, new UserLoggedInEventArgs(obj as UserLoggedInEvent));
        }
        
        public event UserLoggedOutEventHandler UserLoggedOut;

        protected override void OnUserLoggedOut()
        {
            var handler = UserLoggedOut;
            //var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(SubjectState.Key);
            //dynamic obj = JsonSerializer.DeserializeEvent(SubjectState.Value, type);
            if (handler != null) handler(this, new UserLoggedOutEventArgs());
        }

        public event CloseCAALHPEventHandler CloseCAALHP;

        protected override void OnCloseCAALHP()
        {
            var handler = CloseCAALHP;
            if (handler != null)
                handler(this, new EventArgs());
        }
    }

}
