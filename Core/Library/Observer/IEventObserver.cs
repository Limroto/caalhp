﻿using System.Collections.Generic;

namespace caalhp.Core.Library.Observer
{
    public interface IEventObserver
    {
        void Update(KeyValuePair<string,string> theEvent);
        void NewEventTypeUpdate(string newFqns);
    }
}