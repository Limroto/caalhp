using System.Collections.Generic;
using caalhp.Core.Library.Config;
using caalhp.Core.Library.Hosts;
using caalhp.Core.Library.Managers;
using caalhp.Core.Library.State;
using caalhp.Core.Library.Visitor;

namespace caalhp.Core.Library
{
    public interface ICAALHP : IHostVisitable
    {
        void StartApp(string appName);
        void CloseApp(string appName);
        List<PluginConfig> GetListOfInstalledApps();
        IList<PluginConfig> GetListOfInstalledDeviceDrivers();
        void ActivateDeviceDrivers();
        IList<IHostManager> HostManagers { get; }
        EventManager EventManager { get; }
        IUserState UserState { get; set; }
        IList<PluginConfig> GetListOfRunningApps();
    }
}