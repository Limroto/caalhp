﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using caalhp.Core.Library.Config;
using caalhp.Core.SoaIce.Host.Processes;
using System.Threading;

namespace caalhp.Core.SoaIce.Host.LifeCycleManagers
{
    public class DeviceDriverLifeCycleManager : LifeCycleManager
    {
        public Dictionary<int, DriverProcess> DeviceDriverDictionary { get; private set; }

        public DeviceDriverLifeCycleManager(string pluginRoot)
            : base(pluginRoot)
        {
            DeviceDriverDictionary = new Dictionary<int, DriverProcess>();
        }

        public override void StopPlugins()
        {
            KeepAliveTimer.Stop();
            ShouldKeepAlive = false;
            foreach (var deviceDriverProcess in DeviceDriverDictionary.Values.Where(deviceDriverProcess =>          deviceDriverProcess.DeviceDriver != null))
            {
                try
                {
                    //We expect communication to break after this call, so we will not wait for a response.
                    deviceDriverProcess.DeviceDriver.begin_ShutDown();
                }
                catch (Exception e) 
                {
                    Console.WriteLine("Error during StopPlugins: " + e.Message);
                }
           }

            //Allow for a grace period of 300 ms for applications to gracefully shutdown before killing them via the OS
            Thread.Sleep(300);

          foreach (var deviceDriverProcess in DeviceDriverDictionary.Values.Where(deviceDriverProcess =>          deviceDriverProcess.DeviceDriver != null))
          {
                    try
                    {
                        //We expect communication to break after this call, so we will not wait for a response.
                        Process.GetProcessById(deviceDriverProcess.ProcessId).Kill();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Process "+ deviceDriverProcess.ProcessId+" was already closed - as planned: " + e.Message);
                    }
          }
            }

        protected override int GetRestartCounter(int processId)
        {
            return DeviceDriverDictionary.ContainsKey(processId) ? DeviceDriverDictionary[processId].RestartCount : int.MaxValue;
        }

        protected override void IncrementRestartCounter(int processId)
        {
            if (DeviceDriverDictionary.ContainsKey(processId)) DeviceDriverDictionary[processId].RestartCount++;
        }

        protected override void ReactivatePlugin(int key)
        {
            var config = DeviceDriverDictionary[key].Config;
            if (config == null) return;
            var filename = Path.Combine(config.Directory, config.Config.RunFiles.First());
            if (!File.Exists(filename)) return;

            KillPredesseors(filename);
            var process = Process.Start(filename);
            if (process == null) return;
            var pluginProcess = new DriverProcess(process, config) { RestartCount = DeviceDriverDictionary[key].RestartCount };
            DeviceDriverDictionary.Add(process.Id, pluginProcess);
            DeviceDriverDictionary.Remove(key);
            
            Console.WriteLine("reactivated app: " + config.Config.PluginName);
            try
            {
                if (Process.GetProcesses().Any(p => p.Id == key))
                {
                    var oldProcess = Process.GetProcessById(key);
                    if (oldProcess != null) oldProcess.Kill();
                }
            }
            catch 
            {
                Console.WriteLine("Attempted to kill the old version of the reactivated process: "+key);
            }
        }

        protected override void DeactivatePlugin(int processId)
        {
            try
            {
                if (!DeviceDriverDictionary.ContainsKey(processId)) return;
                var view = DeviceDriverDictionary[processId].DeviceDriver;
                if (view == null) return;
                DeviceDriverDictionary[processId].DeviceDriver = null;
                view.ShutDown();
            }
            catch (Exception)
            {
                Console.WriteLine("plugin was already closed");
            }
            finally
            {
                if (Process.GetProcesses().Any(pr => pr.Id == processId))
                    Process.GetProcessById(processId).Kill();
            }
        }

        protected override void KeepAliveTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            foreach (var deviceDriverProcess in DeviceDriverDictionary)
            {
                var process = deviceDriverProcess;
                Task.Run(() =>
                {
                    if (process.Value == null || process.Value.DeviceDriver == null) return;
                    try
                    {
                        if (!process.Value.DeviceDriver.IsAlive())
                        {
                            RestartPlugin(process.Key);
                        }
                    }
                    catch (Exception)
                    {
                        RestartPlugin(process.Key);
                    }
                });
            }
        }

        public override async Task ActivatePlugin(PluginConfig plugin)
        {
            await Task.Run(() =>
            {
                try
                {
                    //Debugger.Break();
                    if (PluginIsActive(plugin)) return;
                    //start the first file
                    //var process = Process.Start(Path.Combine(plugin.Directory, plugin.Config.RunFiles.First()));
                    //var process = Process.Start(Path.Combine(plugin.Directory, plugin.Config.RunFiles.First())); 
                    var plugInToLaunch = Path.Combine(plugin.Directory, plugin.Config.RunFiles.First());
                    ProcessStartInfo info = new ProcessStartInfo();
                    info.WindowStyle = ProcessWindowStyle.Minimized;
                    info.UseShellExecute = true;
                    //info.WorkingDirectory = plugin.Directory;
                    info.FileName = plugInToLaunch;
                    KillPredesseors(plugInToLaunch);
                    Process process = new Process();
                    process.StartInfo = info;
                    process.Start();

                    var pluginProcess = new DriverProcess(process, plugin);
                    if (process != null)
                    {
                        DeviceDriverDictionary.Add(process.Id, pluginProcess);
                        Console.WriteLine("activated device driver: " + plugin.Config.PluginName);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("An error occured while activating a plugin in the DeviceDriverLifeCycleManager.cs " + e.Message);
                }

            });
        }

        private bool PluginIsActive(PluginConfig config)
        {
            return DeviceDriverDictionary.Values.Any(driverProcess => driverProcess.Config.Directory.Equals(config.Directory));
        }

        /*public Dictionary<int, ProcessInfo> GetRunningPluginsProcessInfo()
        {
            var collection = new Dictionary<int, ProcessInfo>();
            foreach (var record in DeviceDriverDictionary)
            {
                var counter = record.Value.PerfCounter;
                var processInfo = new ProcessInfo
                {
                    ProcessId = record.Value.ProcessId,
                    Name = record.Value.DeviceDriver.ToString()
                };
                counter.CategoryName = "Process";

                counter.CounterName = "% Processor Time";
                processInfo.PercentProcessorTime = counter.NextValue();

                counter.CounterName = "Private Bytes";
                processInfo.PrivateBytes = (Int64)counter.NextValue();
                collection.Add(record.Key, processInfo);
            }
            return collection;
        }*/
    }
}