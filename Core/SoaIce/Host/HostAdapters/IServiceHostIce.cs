﻿using caalhp.Core.Library.Hosts;
using Ice;

namespace caalhp.Core.SoaIce.Host.HostAdapters
{
    public interface IServiceHostIce : IServiceHost
    {
        void RegisterService(string name, string category, int processId, Current current__);
    }
}