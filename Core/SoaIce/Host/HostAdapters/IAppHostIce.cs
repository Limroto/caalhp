﻿using caalhp.Core.Library.Hosts;
using Ice;

namespace caalhp.Core.SoaIce.Host.HostAdapters
{
    public interface IAppHostIce : IAppHost
    {
        void RegisterApp(string name, string category, int processId, Current current__);
    }
}