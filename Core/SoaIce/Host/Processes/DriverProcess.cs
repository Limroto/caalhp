﻿using System.Collections.Generic;
using System.Diagnostics;
using caalhp.Core.Library.Config;
using CAALHP.SOAICE.Contracts;

namespace caalhp.Core.SoaIce.Host.Processes
{
    public class DriverProcess : BaseProcess
    {
        public IDeviceDriverContractPrx DeviceDriver { get; set; }
        public DriverProcess(Process process, PluginConfig config) : base(process, config) { }
        public override void Update(KeyValuePair<string, string> theEvent)
        {
            if (DeviceDriver != null) DeviceDriver.begin_Notify(theEvent.Key, theEvent.Value);
        }

        public override void NewEventTypeUpdate(string newFqns)
        {
            //throw new System.NotImplementedException();
        }
    }
}