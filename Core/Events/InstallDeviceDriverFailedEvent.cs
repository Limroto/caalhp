﻿namespace caalhp.Core.Events
{
    public class InstallDeviceDriverFailedEvent : Event
    {
        public string DeviceDriverFileName { get; set; }
        public string Reason { get; set; }
    }
}
