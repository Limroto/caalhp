﻿namespace caalhp.Core.Events
{
    public class ErrorEvent : Event
    {
        public string ErrorMessage { get; set; }
    }
}
