﻿using System;
using System.Collections.Generic;
using caalhp.Core.Events.Types;

namespace caalhp.Core.Events.UserServiceEvents
{
    [Obsolete]
    public class UserUpdateCompleteEvent: Event
    {
        public List<UserProfile> UserList { get; set; }
    }
}
