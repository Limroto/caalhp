﻿using System;

namespace caalhp.Core.Events.UserServiceEvents
{
    public class FacialErrorEvent: Event
    {
        public String Message { get; set; }
    }
}
