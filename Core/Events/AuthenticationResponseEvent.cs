﻿using System;
using caalhp.Core.Events.Types;

namespace caalhp.Core.Events
{
    [Obsolete]
    public class AuthenticationResponseEvent
    {
        public string Username { get; set; }
        public Boolean Response {get; set;}
        public RequestTypes ReqType { get; set; }
        public string Message { get; set; }

    }
}
