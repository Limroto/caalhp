﻿using System;

namespace caalhp.Core.Events
{
    public abstract class Event
    {
        public int CallerProcessId { get; set; }
        public string CallerName { get; set; }
        public DateTime Timestamp { get; set; }

        protected Event()
        {
            Timestamp = DateTime.Now;
        }
    }
}