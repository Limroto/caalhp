using caalhp.Core.Events.Types;

namespace caalhp.Core.Events
{
    public class CurrentUserResponseEvent : Event
    {
        public User CurrentUser { get; set; }
    }
}