﻿namespace caalhp.Core.Events
{
    public class DownloadAppCompletedEvent : Event
    {
        public string AppName { get; set; }
        public string FileName { get; set; }
    }
}