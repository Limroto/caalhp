﻿namespace caalhp.Core.Events
{
    public class DownloadAppEvent : Event
    {
        public string AppName { get; set; }
        public string FileName { get; set; }
    }
}