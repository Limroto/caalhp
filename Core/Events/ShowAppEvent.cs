﻿namespace caalhp.Core.Events
{
    public class ShowAppEvent : Event
    {
        public string AppName { get; set; }
    }
}
