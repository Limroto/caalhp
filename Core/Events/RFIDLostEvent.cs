﻿namespace caalhp.Core.Events
{
    public class RFIDLostEvent : Event
    {
        public string Tag { get; set; }
    }
}
