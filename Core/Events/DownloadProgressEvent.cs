﻿namespace caalhp.Core.Events
{
    public class DownloadProgressEvent : Event
    {
        public string FileName { get; set; }
        public int PercentDone { get; set; }
    }
}
