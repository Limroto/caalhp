﻿using caalhp.Core.Events.Types;

namespace caalhp.Core.Events
{
    public class UserLoggedInEvent: Event
    {
        public User User { get; set; }
        public string PhotoRoot { get; set; }
    }
}
