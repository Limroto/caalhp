﻿namespace caalhp.Core.Events
{
    public class InitializingEvent : Event
    {
        public string Name { get; set; }
    }
}
