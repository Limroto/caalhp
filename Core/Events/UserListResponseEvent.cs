﻿using System;
using System.Collections.Generic;
using caalhp.Core.Events.Types;

namespace caalhp.Core.Events
{
    [Obsolete]
    public class UserListResponseEvent : Event
    {
        public string PhotoRoot { get; set; }
        public List<UserProfile> Users { get; set; }
    }
}
