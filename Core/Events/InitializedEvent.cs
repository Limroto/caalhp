﻿namespace caalhp.Core.Events
{
    public class InitializedEvent : Event
    {
        public string Name { get; set; }
    }
}
