﻿namespace caalhp.Core.Events
{
    public class BloodPressureMeasurementEvent : Event
    {
        public double Diastolic { get; set; }
        public double Systolic { get; set; }
    }
}