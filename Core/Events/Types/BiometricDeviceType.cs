﻿using System;

namespace caalhp.Core.Events.Types
{
    [Obsolete]
    public enum BiometricDeviceType
    {
        Facial,
        Finger,
        Voice
    }
}