﻿using System;

namespace caalhp.Core.Events.Types
{
    [Obsolete]
    public enum CareStoreUserLevel
    {
        Caretaker,
        Citizen
    }
}