﻿namespace caalhp.Core.Events.Types
{
    public class User
    {
        public string Id { get; set; }
        public UserRole Role { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhotoUrl { get; set; }
    }
}
