using System;
using caalhp.Core.Contracts;
using CAALHP.SOAICE.Contracts;

namespace caalhp.IcePluginAdapters
{
    public class DeviceDriverHostToCAALHPContractAdapter : IDeviceDriverHostCAALHPContract
    {
        private IDeviceDriverHostContractPrx _deviceDriverHostContract;

        public DeviceDriverHostToCAALHPContractAdapter(IDeviceDriverHostContractPrx host)
        {
            if (host == null) throw new ArgumentNullException("host");
            Console.WriteLine("Constructing DeviceDriverHostToCAALHPContractAdapter");
            _deviceDriverHostContract = host;
            try
            {
                Console.WriteLine("_deviceDriverHostContract.GetHost");
                Host = new HostToCAALHPContractAdapter(_deviceDriverHostContract);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public IHostCAALHPContract Host { get; set; }
    }
}