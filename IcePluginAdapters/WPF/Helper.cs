﻿using System.Windows;
using System.Security.Permissions;
using System.Windows.Threading;

namespace caalhp.IcePluginAdapters.WPF
{
    public static class Helper
    {
        public static void BringToFront()
        {

            var window = Application.Current.MainWindow;

            //System.Media.SystemSounds.Beep.Play();

            DoEvents(); //Allow Message Pump to Run

            if (!window.IsVisible)
            {
                window.Show();
            }
            window.WindowState = WindowState.Maximized;
            window.Activate();
            window.Topmost = true;
            window.Topmost = false;
            window.Focus();
            
            DoEvents(); //Allow Message Pump to Run
  
        }
        
     /// <summary>
        /// 
        /// This is the WPF version of the WinForm Application.DoEvents that will allow to execute the message pump of an idle application
        /// </summary>
        [SecurityPermissionAttribute(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        public static void DoEvents()
        {
            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
                new DispatcherOperationCallback(ExitFrame), frame);
            Dispatcher.PushFrame(frame);
        }

        /// <summary>
        /// Helper method for DoEvents()
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        public static object ExitFrame(object f)
        {
            ((DispatcherFrame)f).Continue = false;

            return null;
        }
    }
}
