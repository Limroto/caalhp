CAALHP
==================

Main program, which needs to run for any of the following projects to run.

## Purpose of the fork
This fork is created for the R&D project for Rasmus Bækgaard, 10893.
The project is to design and implement a "decision service" for the CAALHP.

It will look at all the incoming events and determine if they are false positives or valid events for triggering an action.

Designs of this can be found [here on LucidChart](https://www.lucidchart.com/invitations/accept/8376fe8c-1d1b-491a-9132-69020a902cf6).