﻿using System;
using System.Collections.Generic;
using System.Threading;
using caalhp.Core.Library;
using caalhp.Core.Library.Brokers;
using caalhp.Core.Library.Config;
using caalhp.Core.Library.Hosts;
using caalhp.Core.Library.Managers;
using caalhp.Core.Library.Observer;
using caalhp.Core.Library.State;
using caalhp.Core.Library.Visitor;
using caalhp.Core.SoaIce.IceHostManager;

namespace caalhp.Console
{
    public class CAALHP : ICAALHP, IDisposable
    {
        public IUserState UserState { get; set; }
        public IList<IHostManager> HostManagers { get; private set; }
        public EventManager EventManager { get; private set; }
        //private ILifeCycleManager _lifeCycleManager;
        private ICAALHPBroker _broker;
            
        public CAALHP()
        {
            Closing = false;
            _broker = new CAALHPBroker(this);
            UserState = new NotLoggedInState(this);
            EventManager = new EventManager();
            HostManagers = new List<IHostManager>();
            HostManagers.Add(new IceHostManager(EventManager, _broker));
            //HostManagers.Add(new MafHostManager.MafHostManager(EventManager, _broker));
            Thread.Sleep(2000);
            EventManager.EventSubject.UserLoggedIn += EventSubject_UserLoggedIn;
            EventManager.EventSubject.UserLoggedOut += EventSubject_UserLoggedOut;
            EventManager.EventSubject.CloseCAALHP += EventSubject_CloseCAALHP;
            
        }

        void EventSubject_CloseCAALHP(object sender, EventArgs e)
        {
            Closing = true;
        }

        public bool Closing { get; set; }

        void EventSubject_UserLoggedIn(object sender, UserLoggedInEventArgs e)
        {
            UserState.UserLoggedIn(e.UserEvent.User);
        }

        void EventSubject_UserLoggedOut(object sender, UserLoggedOutEventArgs e)
        {
            UserState.UserLoggedOut();
        }

        public void StartApp(string appName)
        {
            foreach (var hostManager in HostManagers)
            {
                hostManager.AppHost.ShowLocalApp(appName);
            }
        }

        public void CloseApp(string appName)
        {
            foreach (var hostManager in HostManagers)
            {
                hostManager.AppHost.CloseApp(appName);
            }
        }

        public List<PluginConfig> GetListOfInstalledApps()
        {
            var list = UserState.GetListOfInstalledApps() as List<PluginConfig>;
            return list;
        }

        public IList<PluginConfig> GetListOfRunningApps()
        {
            var apps = new List<PluginConfig>();
            foreach (var hostManager in HostManagers)
            {
                apps.AddRange(hostManager.AppHost.GetLocalRunningApps());
            }
            return apps;
        }

        public IList<PluginConfig> GetListOfInstalledDeviceDrivers()
        {
            var deviceDrivers = new List<PluginConfig>();
            foreach (var hostManager in HostManagers)
            {
                deviceDrivers.AddRange(hostManager.DeviceDriverHost.GetLocalInstalledDeviceDrivers());
            }
            return deviceDrivers;
        }

        public void ActivateDeviceDrivers()
        {
            foreach (var hostmanager in HostManagers)
            {
                if (hostmanager.DeviceDriverHost != null) hostmanager.DeviceDriverHost.ActivateDrivers();
            }
        }

        public void Dispose()
        {
            EventManager.EventSubject.UserLoggedIn -= EventSubject_UserLoggedIn;
            EventManager.EventSubject.UserLoggedOut -= EventSubject_UserLoggedOut;
            foreach (var hostmanager in HostManagers)
            {
                if (hostmanager.AppHost != null) hostmanager.AppHost.ShutDown();
                if (hostmanager.DeviceDriverHost != null) hostmanager.DeviceDriverHost.ShutDown();
                if (hostmanager.ServiceHost != null) hostmanager.ServiceHost.ShutDown();
            }
        }

        public void Accept(IHostVisitor visitor)
        {
            visitor.Visit(this);
            foreach (var hostManager in HostManagers)
            {
                hostManager.Accept(visitor);
            }
        }
    }
}
