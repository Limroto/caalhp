﻿using System;

namespace caalhp.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                System.Console.WriteLine("Starting CAALHP");
                using (var caalhp = new CAALHP())
                {
                    try
                    {
                        System.Console.WriteLine("System running...");
                        System.Console.WriteLine("Press 'q' to close CAALHP");

                        //string a = null; -- used to simulate an exception occurring during runtime
                        //a.CopyTo(0, null, 1, 1); -- used to simulate an exception occurring during runtime
                        while (!caalhp.Closing)
                        {
                            if (!System.Console.KeyAvailable) continue;
                            var key = System.Console.ReadKey(true);
                            if (key.Key != ConsoleKey.Q) continue;

                            caalhp.Closing = true;
                        }

                        System.Console.WriteLine("closing!!!");
                    }
                    catch (Exception e)
                    {
                        System.Console.WriteLine("Error in CAALHP: " + e.Message);
                        caalhp.Closing = true;
                        //caalhp.Dispose();
                    }
                    finally
                    {
                        caalhp.Dispose();
                    }
                    //caalhp.Dispose();
                }
                System.Console.WriteLine("closed.");
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("Error while closing the CAALHP: " + ex.Message);
            }
            finally
            {
                System.Environment.Exit(0);
            }
            }


    }
}
